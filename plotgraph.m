%M0_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_0_result.csv', 1, 0);
M1_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/pc/mean/moyenne_0_result.csv', 1, 0);
M2_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_0_result.csv', 1, 0);

%M2_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_2_cost.csv', 1, 0);
%{
M3_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_3_cost.csv', 1, 0);
M4_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_4_cost.csv', 1, 0);
M5_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_5_cost.csv', 1, 0);
M6_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_6_cost.csv', 1, 0);
M7_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_7_cost.csv', 1, 0);
M8_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_8_cost.csv', 1, 0);
M9_cost = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_9_cost.csv', 1, 0);

M0_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_0_duration.csv', 1, 0);
M1_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_1_duration.csv', 1, 0);
M2_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_2_duration.csv', 1, 0);
M3_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_3_duration.csv', 1, 0);
M4_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_4_duration.csv', 1, 0);
M5_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_5_duration.csv', 1, 0);
M6_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_6_duration.csv', 1, 0);
M7_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_7_duration.csv', 1, 0);
M8_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_8_duration.csv', 1, 0);
M9_duration = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_9_duration.csv', 1, 0);

M0_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_0_result.csv', 1, 0);
M1_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_1_result.csv', 1, 0);
M2_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_2_result.csv', 1, 0);
M3_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_3_result.csv', 1, 0);
M4_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_4_result.csv', 1, 0);
M5_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_5_result.csv', 1, 0);
M6_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_6_result.csv', 1, 0);
M7_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_7_result.csv', 1, 0);
M8_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_8_result.csv', 1, 0);
M9_result = csvread('/home/nicolas/Documents/ENIB/Cours/Argentine/PFE/mean/moyenne_9_result.csv', 1, 0);
%}
order = 1;
framelen = 11;
M0_cost_filter=sgolayfilt(M0_cost(:,end),order,framelen);
M1_cost_filter=sgolayfilt(M1_cost(:,end),order,framelen);
M2_cost_filter=sgolayfilt(M2_cost(:,end),order,framelen);
%M2_cost_filter=sgolayfilt(M2_cost(:,end),order,framelen);

%{
M3_cost_filter=sgolayfilt(M3_cost(:,end),order,framelen);
M4_cost_filter=sgolayfilt(M4_cost(:,end),order,framelen);
M5_cost_filter=sgolayfilt(M5_cost(:,end),order,framelen);
M6_cost_filter=sgolayfilt(M6_cost(:,end),order,framelen);
M7_cost_filter=sgolayfilt(M7_cost(:,end),order,framelen);
M8_cost_filter=sgolayfilt(M8_cost(:,end),order,framelen);
M9_cost_filter=sgolayfilt(M9_cost(:,end),order,framelen);
%}
figure
%{
subplot(2, 2, 1);
plot(M0_cost(:,end), 'r');
xlabel('Iteraciones');
ylabel('Cost');
subplot(2, 2, 3);
plot(M0_cost_filter(:,end), 'b')
xlabel('Iteraciones');
ylabel('Cost')
%}
subplot(3, 1, 1);
plot(M0_cost_filter(:,end), 'r');
xlabel('Iteraciones');
ylabel('Result');
subplot(3, 1, 2);
plot(M1_cost_filter(:,end), 'b')
xlabel('Iteraciones');
ylabel('Result')
subplot(3, 1, 3);
plot(M2_cost_filter(:,end), 'b')
xlabel('Iteraciones');
ylabel('Result')
%plot(M2_cost_filter(:,end), 'b')

%{
plot(M3_cost_filter(:,end), 'y')
plot(M4_cost_filter(:,end), 'm')
plot(M5_cost_filter(:,end), 'g')
plot(M6_cost_filter(:,end), 'g')
plot(M7_cost_filter(:,end), 'b')
plot(M8_cost_filter(:,end), 'y')
plot(M9_cost_filter(:,end), 'm')
%}

