# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

"""

Create genes randomly

"""
from random import randrange, seed, choice

from application.settings import MAXIMUM_TASK, MAXIMUM_TEAM, \
    MINIMUM_TIME_UNIT, \
    MAXIMUM_TIME_UNIT, EXECUTION_MODE, COMPETENCE_LEVEL


def init_task_gene(x):
    """Create task genes with randoms parameters"""

    initial_task_gene = []
    seed()

    if x > MAXIMUM_TASK:
        print("The number of genes for task you asked is too big, max: " +
              str(MAXIMUM_TASK))
    else:
        for i in range(0, x):
            gene_time_unit = randrange(MINIMUM_TIME_UNIT,
                                       MAXIMUM_TIME_UNIT, 1)

            gene_execution_mode = choice(EXECUTION_MODE)

            gene_competence_level = choice(COMPETENCE_LEVEL)

            task_gene = [i, gene_time_unit, gene_execution_mode,
                         gene_competence_level]

            initial_task_gene.append(task_gene)

    return initial_task_gene


def init_team_gene(x):
    """Create task genes with randoms parameters"""

    seed()
    initial_team_gene = []

    if x > MAXIMUM_TEAM:
        print("The number of genes for team you asked is too big, max: " +
              str(MAXIMUM_TEAM))
    else:
        "Generate at least one team with min and max level"
        gene_competence_level = COMPETENCE_LEVEL[-1]
        gene_cost = gene_competence_level * 1000
        team_gene = [0, gene_competence_level, gene_cost]
        initial_team_gene.append(team_gene)
        gene_competence_level = COMPETENCE_LEVEL[0]
        gene_cost = gene_competence_level * 1000
        team_gene = [0, gene_competence_level, gene_cost]
        initial_team_gene.append(team_gene)

        for i in range(2, x):
            gene_competence_level = choice(COMPETENCE_LEVEL)

            gene_cost = gene_competence_level * 1000

            team_gene = [i, gene_competence_level, gene_cost]

            initial_team_gene.append(team_gene)

    return initial_team_gene


if __name__ == "__main__":
    initial_genes = init_task_gene(5000), init_team_gene(15)
    initial_genes = init_task_gene(1001), init_team_gene(8)
    initial_genes = init_task_gene(3), init_team_gene(5)
    print("Initial task genes generated: " + str(initial_genes[0]))
    print("Initial team genes generated: " + str(initial_genes[1]))
