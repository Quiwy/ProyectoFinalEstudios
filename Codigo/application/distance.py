# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

import random
from numpy import zeros

from application.settings import DISTANCE


def generate_distance_matrix(team_gene):
    number_team = len(team_gene)
    distance_matrix = zeros((number_team, number_team))
    for i in range(0, number_team):
        for k in range(0, number_team):
            if k < i or k == i:
                pass
            else:
                distance_matrix[i, k] = random.choice(DISTANCE)
        for k in range(0, number_team):
            if k > i or k == i:
                pass
            else:
                distance_matrix[i, k] = distance_matrix[k, i]

    return distance_matrix


def get_distance(distance_matrix, team_a, team_b):
    return distance_matrix.item(team_a, team_b)


if __name__ == '__main__':
    initial_genes = [[0, 6, 2, 5], [1, 2, 1, 3], [2, 7, 2, 1], [3, 4, 1, 2],
                     [4, 8, 2, 1], [5, 9, 1, 1], [6, 9, 2, 2], [7, 9, 1, 2],
                     [8, 9, 1, 2], [9, 7, 1, 2], [10, 9, 2, 3], [11, 4, 1, 5],
                     [12, 8, 2, 4], [13, 9, 1, 3], [14, 8, 1, 3]], [
                        [0, 5, 5000], [1, 5, 5000], [2, 3, 3000],
                        [3, 2, 2000], [4, 3, 3000], [5, 3, 3000],
                        [6, 2, 2000]]
    distance_matrix = generate_distance_matrix(initial_genes[1])
    print(distance_matrix)
    print(get_distance(distance_matrix, 1, 5))
