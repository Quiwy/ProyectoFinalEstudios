# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

# !/usr/bin/python
# -*- coding: utf-8 -*-
from ast import literal_eval
from datetime import datetime
import csv


def save_initial_genes_task(initial_genes_task):
    """Save initial genes from file if desired"""
    file = open('../data/initialGenesTask.txt', 'w')
    number_genes = len(initial_genes_task)
    for i in range(0, number_genes):
        file.write(str(initial_genes_task[i][0]) + " " +
                   str(initial_genes_task[i][1]) + " " +
                   str(initial_genes_task[i][2]) + " " +
                   str(initial_genes_task[i][3]) + "\n")
    file.close()


def save_initial_genes_teams(initial_genes_teams):
    """Save initial genes from file if desired"""
    file = open('../data/initialGenesTeam.txt', 'w')
    number_genes = len(initial_genes_teams)
    for i in range(0, number_genes):
        file.write(str(initial_genes_teams[i][0]) + " " +
                   str(initial_genes_teams[i][1]) + " " +
                   str(initial_genes_teams[i][2]) + "\n")
    file.close()


def save_initial_population(population):
    """Save initial population to file"""
    file = open('../data/initialPopulation.txt', 'w')
    for i in range(0, len(population)):
        file.write(str(population[i]) + "\n")
    file.close()


def load_initial_genes_task():
    """Load initial genes task from file if desired"""
    initial_task = []
    path = input("Please indicate the path of the txt file, default "
                 "../data/initialGenesTask.txt : ")
    if path == "":
        filename = '../data/initialGenesTask.txt'
    else:
        filename = path

    try:
        with open(filename, 'r') as file:
            data = file.readlines()
            number_tasks = len(data)
            for line in data:
                words = line.split()
                initial_task.append(words)
        file.close()
        for i in range(0, len(initial_task)):
            for j in range(0, len(initial_task[i])):
                initial_task[i][j] = int(initial_task[i][j])
        return number_tasks, initial_task

    except IOError:
        print("No file named " + filename)
        exit()


def load_initial_genes_team():
    """Load initial genes team from file if desired"""
    initial_team = []
    path = input("Please indicate the path of the txt file, default "
                 "../data/initialGenesTask.txt : ")
    if path == "":
        filename = '../data/initialGenesTeam.txt'
    else:
        filename = path

    try:
        with open(filename, 'r') as file:
            data = file.readlines()
            number_tasks = len(data)
            for line in data:
                words = line.split()
                if isinstance(words, int):
                    initial_team.append(int(words))
                else:
                    initial_team.append(words)
        for i in range(0, len(initial_team)):
            for j in range(0, len(initial_team[i])):
                initial_team[i][j] = int(initial_team[i][j])

        file.close()

        return number_tasks, initial_team
    except IOError:
        print("No file named " + filename)
        exit()


def load_initial_population():
    """Load initial population from file if desired"""
    path = input("Please indicate the path of the txt file, default "
                 "initialPopulation.txt : ")
    if path == "":
        filename = '../data/initialPopulation.txt'
    else:
        filename = path

    try:
        with open(filename, 'r') as file:
            initial_population = [list(literal_eval(line)) for line in file]
        file.close()
        return initial_population
    except IOError:
        print("No file named " + filename)
        exit()


def save_result(mutation_rate, cross_over_rate, k, l, to_save):
    """Save everything"""
    with open("../data/final_" + str(mutation_rate) + "_" + str(
            cross_over_rate) + "_" + str(datetime.now()) + ".csv",
            'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(to_save)
