# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

from application.distance import *
from math import pow


def get_duration(individual, matrix_distance):
    total_duration = 0
    coeff = individual[0][1][1] - individual[0][0][3]
    total_duration = total_duration + individual[0][0][1] * pow(0.75, coeff)
    for i in range(1, len(individual)-1):
        coeff = individual[i][1][1] - individual[i][0][3]
        total_duration = total_duration + pow(0.75, coeff) * individual[i][
            0][1] + get_distance(matrix_distance, individual[i][1][0],
                                 individual[i-1][1][0])*individual[i-1][0][1]

    return round(total_duration, 0)


if __name__ == "__main__":
    individual = [[[0, 6, 3, 3], [5, 3, 3000]], [[1, 2, 1, 3], [2, 3, 3000]],
                  [[2, 7, 2, 5], [4, 1, 1000]], [[3, 4, 1, 2], [3, 2, 2000]],
                  [[4, 8, 2, 1], [5, 3, 3000]], [[5, 9, 1, 1], [2, 3, 3000]],
                  [[6, 9, 2, 2], [5, 3, 3000]], [[7, 9, 1, 2], [2, 3, 3000]],
                  [[8, 9, 1, 2], [3, 2, 2000]], [[9, 7, 1, 2], [0, 5, 5000]],
                  [[10, 9, 2, 3], [4, 3, 3000]],
                  [[11, 4, 1, 5], [1, 5, 5000]],
                  [[12, 8, 2, 4], [0, 5, 5000]],
                  [[13, 9, 1, 3], [2, 3, 3000]],
                  [[14, 8, 1, 3], [1, 5, 5000]], '0']

    genes = [[0, 6, 2, 5], [1, 2, 1, 3], [2, 7, 2, 1], [3, 4, 1, 2],
             [4, 8, 2, 1], [5, 9, 1, 1], [6, 9, 2, 2], [7, 9, 1, 2],
             [8, 9, 1, 2], [9, 7, 1, 2], [10, 9, 2, 3], [11, 4, 1, 5],
             [12, 8, 2, 4], [13, 9, 1, 3], [14, 8, 1, 3]], [
                [0, 5, 5000], [1, 5, 5000], [2, 3, 3000],
                [3, 2, 2000], [4, 3, 3000], [5, 3, 3000]]

    distance = generate_distance_matrix(genes[1])
    print(distance)
    print(get_duration(individual, distance))
