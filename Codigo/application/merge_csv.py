# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

import glob
import os
import pandas as pd


def process_csv():
    results = pd.DataFrame([])
    namedf = []
    dir = os.getcwd()
    os.chdir(dir)
    os.chdir('../..')
    print(dir)
    if not os.path.exists('transposed'):
        os.mkdir('transposed')

    if not os.path.exists('mean'):
        os.mkdir('mean')

    i = 1
    for fname in sorted(glob.glob('data/*.csv')):
        file = pd.read_csv(fname).transpose()
        print(fname)
        file.to_csv("transposed/new" + str(i) + ".csv")
        i += 1

    for i in range(0, 10):

        results = pd.DataFrame([])
        cost = pd.DataFrame([])
        duration = pd.DataFrame([])
        namedf_results = []
        namedf_cost = []
        namedf_duration = []
        for j in range(1, 100):
            file = "transposed/new" + str(i * 100 + j) + ".csv"
            print(file)
            namedf_results.append(pd.read_csv(file, skiprows=0, usecols=[
                5]).fillna(method='ffill'))
            results = pd.concat(namedf_results, axis=1)
            namedf_cost.append(pd.read_csv(file, skiprows=0, usecols=[
                2]).fillna(method='ffill'))
            cost = pd.concat(namedf_cost, axis=1)
            namedf_duration.append(pd.read_csv(file, skiprows=0, usecols=[
                3]).fillna(method='ffill'))
            duration = pd.concat(namedf_duration, axis=1)

        results.fillna(method='ffill', inplace=True)
        cost.fillna(method='ffill', inplace=True)
        duration.fillna(method='ffill', inplace=True)

        moyenne_result = results.copy()
        moyenne_result['average_result'] = moyenne_result.mean(
            numeric_only=True, axis=1)
        moyenne_result.to_csv("mean/moyenne_" + str(i) + "_result.csv",
                              decimal='.')

        moyenne_cost = cost.copy()
        moyenne_cost['average_cost'] = moyenne_cost.mean(
            numeric_only=True, axis=1)
        moyenne_cost.to_csv("mean/moyenne_" + str(i) + "_cost.csv",
                            decimal='.')

        moyenne_duration = duration.copy()
        moyenne_duration['average_duration'] = moyenne_duration.mean(
            numeric_only=True, axis=1)
        moyenne_duration.to_csv("mean/moyenne_" + str(i) + "_duration.csv",
                                decimal='.')


if __name__ == '__main__':
    process_csv()
