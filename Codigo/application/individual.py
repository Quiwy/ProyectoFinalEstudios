# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.


""""

Create individual from genes

"""
import random


def generate_individual(genes):
    random.seed()
    generated_individual = []
    for i in range(0, len(genes[0])):
        rand_team = random.randrange(0, len(genes[1]), 1)
        if genes[0][i][2] == 1:
            while genes[1][rand_team][1] < genes[0][i][3]:
                rand_team = random.randrange(0, len(genes[1]), 1)
            generated_individual.append([genes[0][i], genes[1][rand_team]])
        elif genes[0][i][2] == 2:
            while genes[1][rand_team][1] > genes[0][i][3]:
                rand_team = random.randrange(0, len(genes[1]), 1)
            generated_individual.append([genes[0][i], genes[1][rand_team]])
        else:
            k = 0
            while genes[1][rand_team][1] != genes[0][i][3] and k < 50:
                rand_team = random.randrange(0, len(genes[1]), 1)
                k += 1
            if k == 50:
                rand_team = random.randrange(0, len(genes[1]), 1)
            generated_individual.append([genes[0][i], genes[1][rand_team]])
    generated_individual.append('0')
    return generated_individual


if __name__ == "__main__":
    genes = [[0, 6, 3, 1], [1, 2, 1, 3], [2, 7, 2, 5], [3, 4, 1, 2],
             [4, 8, 2, 1], [5, 9, 1, 1], [6, 9, 3, 2], [7, 9, 1, 2],
             [8, 9, 1, 2], [9, 7, 1, 2], [10, 9, 2, 3], [11, 4, 3, 5],
             [12, 8, 2, 4], [13, 9, 1, 3], [14, 8, 1, 3]], [
                [0, 5, 5000], [1, 5, 5000], [2, 3, 3000],
                [3, 2, 2000], [4, 3, 3000], [5, 3, 3000],
                [6, 1, 1000]]
    individual = generate_individual(genes)
    print(individual)
