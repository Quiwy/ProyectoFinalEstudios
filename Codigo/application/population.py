# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

"""

Create a population with various individuals

"""

from application.individual import generate_individual


def init_population(x, initial_genes):
    "Create the initial population of X combinaisons"
    generated_population = []

    for i in range(0, x):
        generated_individual = generate_individual(initial_genes)
        generated_population.append(generated_individual)

    return generated_population


if __name__ == "__main__":

    initial_genes = [[0, 6, 2, 5], [1, 2, 1, 3], [2, 7, 2, 1], [3, 4, 1, 2],
                     [4, 8, 2, 1], [5, 9, 1, 1], [6, 9, 2, 2], [7, 9, 1, 2],
                     [8, 9, 1, 2], [9, 7, 1, 2], [10, 9, 2, 3], [11, 4, 1, 5],
                     [12, 8, 2, 4], [13, 9, 1, 3], [14, 8, 1, 3]], [
                        [0, 8, 8000], [1, 5, 5000], [2, 3, 3000],
                        [3, 2, 2000], [4, 4, 4000], [5, 3, 3000],
                        [6, 2, 2000]]

    population = init_population(5, initial_genes)
    print(population)
