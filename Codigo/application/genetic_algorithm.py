# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

# !/usr/bin/python# -*- coding: utf-8 -*-
import random
import time

from application.duration import get_duration
from application.cost import get_cost


def fitness(individual, distance, coeff_dur, coeff_cost):
    result = coeff_cost * get_cost(individual) + coeff_dur * get_duration(
        individual, distance)
    individual[len(individual) - 1] = result
    return individual


def mutate(individual, genes):
    """Mutate a gen of an individual"""
    number_individual = len(individual) - 1
    number_team_gene = len(genes[1])

    rand1 = random.randrange(0, number_individual, 1)
    rand_team = random.randrange(0, number_team_gene, 1)
    if individual[rand1][0][2] == 1:
        while individual[rand1][0][3] > genes[1][rand_team][1]:
            rand_team = random.randrange(0, len(genes[1]), 1)
        individual[rand1][1] = genes[1][rand_team]
    elif individual[rand1][0][2] == 2:
        while individual[rand1][0][3] < genes[1][rand_team][1]:
            rand_team = random.randrange(0, len(genes[1]), 1)
        individual[rand1][1] = genes[1][rand_team]
    else:
        k = 0
        while individual[rand1][0][3] != genes[1][rand_team][1] and k < 1000:
            rand_team = random.randrange(0, len(genes[1]), 1)
            k += 1
        if k > 1000:
            rand_team = random.randrange(0, len(genes[1]), 1)
        individual[rand1][1] = genes[1][rand_team]

    return individual


def cross(individual1, individual2):
    """Do crossover with 2 individuals"""
    len_individual = len(individual1) - 1
    new_individual = [None] * (len_individual + 1)
    cross_over_position1 = random.randrange(1, len_individual, 1)
    cross_over_position2 = random.randrange(1, len_individual, 1)

    while cross_over_position2 == cross_over_position1:
        cross_over_position2 = random.randrange(1, len_individual, 1)

    for i in range(0, cross_over_position1):
        new_individual[i] = individual1[i]
    for i in range(cross_over_position1, cross_over_position2):
        new_individual[i] = individual2[i]
    for i in range(cross_over_position2, len_individual):
        new_individual[i] = individual1[i]
    return new_individual


if __name__ == "__main__":
    individual = [[[0, 6, 2, 1], [5, 3, 3000]], [[1, 2, 1, 6], [2, 6, 6000]],
                  [[2, 7, 2, 1], [4, 1, 1000]], [[3, 4, 1, 2], [3, 2, 2000]],
                  [[4, 8, 2, 1], [5, 3, 3000]], [[5, 9, 1, 1], [2, 3, 3000]],
                  [[6, 9, 2, 2], [5, 3, 3000]], [[7, 9, 1, 2], [2, 3, 3000]],
                  [[8, 9, 1, 1], [3, 1, 1000]], [[9, 7, 1, 2], [0, 5, 5000]],
                  [[10, 9, 2, 3], [4, 3, 3000]],
                  [[11, 4, 1, 5], [1, 5, 5000]],
                  [[12, 8, 2, 4], [0, 5, 5000]],
                  [[13, 9, 1, 3], [2, 3, 3000]],
                  [[14, 8, 1, 8], [1, 8, 8000]], '0']

    genes = [[0, 6, 2, 5], [1, 2, 1, 3], [2, 7, 2, 1], [3, 4, 1, 2],
             [4, 8, 2, 1], [5, 9, 1, 1], [6, 9, 2, 2], [7, 9, 1, 2],
             [8, 9, 1, 2], [9, 7, 1, 2], [10, 9, 2, 3], [11, 4, 1, 5],
             [12, 8, 2, 4], [13, 9, 1, 3], [14, 8, 1, 3]], [
                [0, 5, 5000], [1, 1, 1000], [2, 6, 6000],
                [3, 2, 2000], [4, 8, 8000], [5, 3, 3000]]

    mutate(individual, genes)
