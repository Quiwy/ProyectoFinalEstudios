# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

MAXIMUM_ITERATION = 500
MAXIMUM_TASK = 200
MAXIMUM_TEAM = 15
MINIMUM_TIME_UNIT = 1
MAXIMUM_TIME_UNIT = 10
EXECUTION_MODE = [1, 2, 3]
COMPETENCE_LEVEL = [1, 2, 5, 10, 20, 30, 40, 50, 75, 100]
DISTANCE = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
