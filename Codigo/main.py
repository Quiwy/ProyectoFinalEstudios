# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.

from application.population import *
from application.genes import *
from application.duration import *
from application.utils import *
from application.genetic_algorithm import *
from application.merge_csv import *
from franges import frange
from application.settings import MAXIMUM_ITERATION
import sys
import os

cross_over_rate = 2
mutation_rate = 2
number_tasks = MAXIMUM_TASK + 1
number_team = MAXIMUM_TEAM + 1


def get_key(item):
    return item[len(item) - 1]


def process(mutation, crossover, distance, population, genes, k, l, verbose):
    number_iteration = 0
    total_proba = 0
    total_iteration = 0
    number_mutation = 0
    number_crossover = 0
    result = [[], [], [], [], [], []]

    random.seed()
    len_population = len(population)
    print(k, l)
    for i in range(0, len(population)):
        total_proba = total_proba + i
        population[i] = fitness(population[i], distance, k, l)

    population = sorted(population, key=get_key)
    best_version = population[0][len(population[0]) - 1]

    print("Best version at beginning is ", best_version)
    while number_iteration < MAXIMUM_ITERATION:
        for i in range(0, len(population)):
            population[i] = fitness(population[i], distance, k, l)
        population = sorted(population, key=get_key)

        new_population = []

        for i in range(0, len(population)):
            select_rang = i / total_proba

            rand = random.uniform(0, 1)
            if rand <= select_rang:
                new_population.append(population[i])

        while len(new_population) < len_population:
            if random.uniform(0, 1) < mutation:
                rand = random.randrange(0, len_population, 1)
                new_population.append(
                    mutate((population[rand]), genes))
                number_mutation += 1
            elif random.uniform(0, 1) < crossover:
                rand1 = random.randrange(0, len_population, 1)
                rand2 = random.randrange(0, len_population, 1)
                new_population.append(cross((population[rand1]),
                                            population[rand2]))
                new_population[len(new_population) - 1] = fitness(
                    new_population[len(new_population) - 1], distance,
                    k, l)
                number_crossover += 1
            else:
                rand = random.randrange(0, len_population, 1)
                new_population.append(population[rand])

        population = new_population

        if best_version >= population[0][len(population[0]) - 1]:
            best_version = population[0][len(population[0]) - 1]
            print(best_version)
            number_iteration = 0
            total_iteration += 1
        else:
            number_iteration += 1
            total_iteration += 1

        result[0].append(k)
        result[1].append(l)
        result[2].append(get_cost(population[0]))
        result[3].append(get_duration(population[0], distance))
        result[4].append(total_iteration)
        result[5].append(best_version)
    if verbose == 1:
        """print("Best version at end is: ", best_version, "with ",
              population[0])"""
        print("There where ", number_mutation, " mutations, ",
              number_crossover, " crossover and ", total_iteration,
              "iterations")
    else:
        print("There where ", number_mutation, " mutations, ",
              number_crossover, " crossover and ", total_iteration,
              "iterations")
        print("The cost of the best individual was ", get_cost(population[0]))
        print("The duration of the best individual was ",
              get_duration(population[0], distance))
    return result


if __name__ == '__main__':
    if sys.argv[1] == "1":
        if not os.path.exists('../data'):
            os.mkdir('../data')
        mutation_rate = 0.9
        cross_over_rate = 0.9
        initial_genes = init_task_gene(200), init_team_gene(15)
        initial_population = init_population(100, initial_genes)
        distance = generate_distance_matrix(initial_genes[1])
        save_initial_genes_task(initial_genes[0])
        save_initial_genes_teams(initial_genes[1])
        save_initial_population(initial_population)

        for k in frange(0.0, 1.0, 0.1):
            for a in range(0, 100):
                oposite_k = 1 - k
                to_save = process(mutation_rate, cross_over_rate, distance,
                                  initial_population, initial_genes, k,
                                  oposite_k, 0)
                save_result(mutation_rate, cross_over_rate, oposite_k, k,
                            to_save)

        process_csv()

    else:
        #####################################################################
        try:
            while mutation_rate >= 1 or mutation_rate <= 0:
                mutation_rate = float(input("Which mutation rate do you want?"
                                            " 0-1 "))
        except ValueError:
            print("It's not a float")
        try:
            while cross_over_rate >= 1 or cross_over_rate <= 0:
                cross_over_rate = float(
                    input("Which crossover rate do you want?"
                          " 0-1 "))
        except ValueError:
            print("It's not a float")

        #####################################################################
        '''
        Ask to load initial individuals already
        existing or number of components
        '''

        load = input("Do you want to load a file with initial genes "
                     " and population? [y/N] ")
        if load == "Y" or load == "y":
            initial_genes = []
            number_tasks, initial_task_genes = load_initial_genes_task()
            number_genes, initial_team_genes = load_initial_genes_team()

            initial_genes.append(initial_task_genes)
            initial_genes.append(initial_team_genes)
            initial_population = load_initial_population()

        else:
            while number_tasks > MAXIMUM_TASK or number_team > MAXIMUM_TEAM:
                try:
                    number_team = int(input(
                        "Please enter the number of teams < " + str(
                            MAXIMUM_TEAM) + ": "))
                    number_tasks = int(
                        input("Please enter the number of tasks "
                              "<" + str(MAXIMUM_TASK) + ": "))

                    initial_genes = init_task_gene(
                        number_tasks), init_team_gene(
                        number_team)
                except ValueError:
                    print("It's not an int")

            save_initial_genes_task(initial_genes[0])
            save_initial_genes_teams(initial_genes[1])
        ######################################################################

        if 'initial_population' in locals():
            pass
        else:
            initial_population = init_population(50, initial_genes)
            save_initial_population(initial_population)

        distance = generate_distance_matrix(initial_genes[1])

        ######################################################################

        process(mutation_rate, cross_over_rate, distance, initial_population,
                initial_genes)
