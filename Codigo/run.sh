#!/usr/bin/env bash
# Copyright (C) 2017 Nicolas Caillet <quiwy@quiwy.ninja>
#
# This file is part of PFE.
#
# PFE is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PFE is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with PFE.  If not, see <http://www.gnu.org/licenses/>.
#

python3 -c 'import sys;(print("Python 3.4 or newer is required") and exit
(1)) if sys.version_info < (3, 4) else exit(0)' || exit 1
VENV="Codigo/.pfe-venv"
DIR=$(dirname "$0")
VENV_COMMAND="python3 -m venv"
PYTHON="$VENV/bin/python"


if [ -e "$VENV" ]; then
	echo 'Trying to upgrade the venv'
	$VENV_COMMAND --upgrade "$VENV"
else
	 echo 'Creating the venv'
	 $VENV_COMMAND "$VENV"
fi

. "$VENV/bin/activate"

cd $DIR

if type pip3 2>/dev/null; then
   pip3 install -r "requirements.txt" --upgrade
else
   echo 'python3-pip is required, please install it'
   exit 1
fi

mkdir ../result
/bin/sh -c "../$PYTHON -OO main.py 1"